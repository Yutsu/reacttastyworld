# TastyWorld Website
![Design homepage of the TastyWorld Website](./src/assets/homepage.png)

## Description
This is a TastyWorld website project developed using react.

//Disclaimer : for now, there is a bug when you want to fave a meal, you have to refresh the website to have the meal in your card + delete "idMealFav" in your local storage

## Technologies Used

React

React-Router-Dom

React-icons

React-player

Hooks

Local storage

Scss

Swiper

Axios

## Installation

git clone https://gitlab.com/Yutsu/reacttastyworld.git

npm install

npm start

## Resources

**Enzo Abramo**

https://www.pexels.com/tr-tr/fotograf/beyaz-daktilo-tuslarinin-yakin-cekim-fotografi-1298626/

**Valentin antonussi**

https://blog.coveo.com/fr/pexels-valentin-antonucci-1275393-2/

**Ready made**

https://www.pexels.com/photo/ingredients-on-the-table-3851088/

Illustrations comes from undraw.io