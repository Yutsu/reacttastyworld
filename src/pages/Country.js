import React, {useState, useEffect} from 'react';
import axios from 'axios';
import img from '../assets/world.svg';
import Card from '../components/cards/Card';

const Country = () => {
    const [countryMeal, setCountryMeal] = useState([]);
    const [displayMealCountry, setDisplayMealCountry] = useState([]);
    const [nameCountry, setNameCountry] = useState("");

    const URL_COUNTRY = 'https://www.themealdb.com/api/json/v1/1/list.php?a=list';
    const URL_COUNTRY_MEAL = 'https://www.themealdb.com/api/json/v1/1/filter.php?a=';

    const displayCountry = () => {
        axios
            .get(URL_COUNTRY)
            .then(result => setCountryMeal(result.data.meals))
            .catch(error => console.error(error));
    }

    const clickCountry = (e) => {
        let country = e.target.textContent;
        setNameCountry(country);
        axios
            .get(`${URL_COUNTRY_MEAL}${country}`)
            .then(result => setDisplayMealCountry(result.data.meals))
            .catch(error => console.error(error));
    }

    useEffect(() => {
        displayCountry();
    }, []);

    return (
        <div className="country">
            <div className="country_img-bg"></div>
            <div className="countries-flag">
                {countryMeal.map(meal=> (
                    <ul key={meal.strArea}>
                        <li onClick={(e) => clickCountry(e)}>{meal.strArea}</li>
                    </ul>
                ))}
            </div>
            <div className="country-container">
                <div className="country-name">
                    {!nameCountry && <h1>Choose the country to see all the meal !</h1>}
                    {nameCountry && <h1>{nameCountry}</h1>}
                </div>
                <div className="countries-meals">
                    {!nameCountry && <img src={img} alt="background world" className="img-country"/>}
                    {displayMealCountry.map(meal => (
                        <Card dataMeal={meal} key={meal.idMeal}/>
                    ))}
                </div>
            </div>
        </div>
    );
};

export default Country;