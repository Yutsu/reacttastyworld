import img from '../assets/po.jpg';
import HomeRandom from '../components/section/HomeRandom';
import { Link } from 'react-router-dom';

const Home = () => {
    return (
        <div className="home">
            <div className="home_img-bg">
                <img src={img} alt="background homepage with meals"/>
                <div className="home_accroche">
                    <p>Yummy food that make <br/> you hungry !</p>
                    <Link to="/meals">
                        <div className="home_btn">Show more</div>
                    </Link>
                </div>
            </div>
            <HomeRandom />
        </div>
    )
}

export default Home;