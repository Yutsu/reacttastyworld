import React from 'react';
import Search from '../components/section/Search';

const Meals = () => {
    return (
        <div className="meals">
            <div className="meal_img-bg"></div>
            <div className="meal-info">
                <div className="filter">
                    <Search />
                </div>
            </div>
        </div>
    );
};

export default Meals;