import React from 'react';
import img from '../assets/not_found.svg'

const NotFound = () => {
    const mystyle = {
        padding: "60px",
        
    };

    return (
        <div className="flex center">
            <img src={img} style={mystyle} alt="route not found"/>
        </div>
    );
};

export default NotFound;