import React, { useState } from 'react';
import img1 from '../assets/letter.svg';
import axios from 'axios';
import Card from '../components/cards/Card';
import NoResult from '../components/global/NoResult';
import {SwiperSlide, Swiper} from 'swiper/react';
import 'swiper/swiper-bundle.css';
import SwiperCore, {Navigation, Pagination} from 'swiper';

SwiperCore.use([Navigation, Pagination]);

const Alphabet = () => {
    const [mealLetter, setMealLetter] = useState([]);
    const [oneLetter, setOneLetter] = useState("");
    const tab =["A","B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M",
                "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
    const URL = 'https://www.themealdb.com/api/json/v1/1/search.php?f=';
    const slides = [];

    const clickLetter = (e) => {
        let letter = e.target.textContent;
        setOneLetter(letter);

        axios
            .get(`${URL}${letter}`)
            .then(result => setMealLetter(result.data.meals))
            .catch(error => console.error(error));
    }

    if(mealLetter){
        mealLetter.map((meal, i) => (
            slides.push(
                <SwiperSlide key={i}>
                    <Card dataMeal={meal} key={meal.idMeal} />
                </SwiperSlide>
            )
        ))
    }

    return (
        <div className="alphabet">
            <div className="alphabet_img-bg"></div>
            <div className="alphabet-container">
                <div className="letter">
                    {tab.map(letter => (
                        <ul key={letter}>
                            <li onClick={(e) => clickLetter(e)}>{letter}</li>
                        </ul>
                    ))}
                </div>
                <div className="alphabet-name">
                    {!oneLetter && <h1>Research by the first letter</h1>}
                    {oneLetter && <h1>{oneLetter}</h1>}
                </div>
                <div className="alphabet-meals">
                    {!oneLetter && <img src={img1} alt="letter" className="img-letter"/>}
                    {oneLetter && !mealLetter && <NoResult />}
                    {oneLetter && mealLetter &&
                        <div className="flex column">
                            <Swiper
                                id="main"
                                navigation
                                pagination
                                spaceBetween={0}
                                slidesPerView={1}
                            >
                                {slides}
                            </Swiper>
                        </div>
                    }
                </div>
            </div>
        </div>
    );
};

export default Alphabet;