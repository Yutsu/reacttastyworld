import Home from "./pages/Home";
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import NotFound from "./pages/NotFound";
import Navbar from "./components/global/Navbar";
import Meals from "./pages/Meals";
import Country from "./pages/Country";
import Alphabet from "./pages/Alphabet";
import CardDetail from "./components/cards/CardDetail";
import Footer from "./components/global/Footer";
import ScrollToTop from "./components/global/ScrollToTop";

function App() {
    return (
        <BrowserRouter>
            <ScrollToTop/>
            <Navbar/>
            <Switch>
                <Route path="/" exact component={Home} />
                <Route path="/meals" exact component={Meals} />
                <Route path="/country" exact component={Country} />
                <Route path="/alphabet" exact component={Alphabet} />
                <Route path="/meal/:slug">
                    <CardDetail />
                </Route>
                <Route component={NotFound} />
            </Switch>
            <Footer />
        </BrowserRouter>
    );
}

export default App;
