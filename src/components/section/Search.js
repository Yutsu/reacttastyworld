import React, { useState, useEffect, useRef }  from 'react';
import axios from 'axios';
import Card from '../cards/Card';
import img from '../../assets/cooking.svg';

const Search = () => {
    const [categories, setCategories] = useState([]);
    const [displayMeal, setDisplayMeal] = useState([]);
    const [nameSearch, setNameSearch] = useState("");
    const inputElement = useRef(null);

    const URL_SEARCH = 'https://www.themealdb.com/api/json/v1/1/search.php?s=';
    const URL_CATEGORIES = 'https://www.themealdb.com/api/json/v1/1/categories.php';
    const URL_CATEGORIES_FILTER = 'https://www.themealdb.com/api/json/v1/1/filter.php?c=';

    const searchFilter = (e) => {
        e.preventDefault();
        let inputValue = e.target.value;
        setNameSearch(inputValue);

        axios
            .get(`${URL_SEARCH}${inputValue}`)
            .then(result => {
                (result.data.meals !== null) ? setDisplayMeal(result.data.meals): setDisplayMeal([]);
            })
            .catch(error => console.error(error));
    }

    const searchCategories = () => {
        axios
            .get(URL_CATEGORIES)
            .then(result => setCategories(result.data.categories))
            .catch(error => console.error(error));
    }

    const clickCategory = (e) => {
        window.scrollTo({
            top: 150,
            left: 0,
            behavior: "smooth"
        });
        let category = e.target.textContent;
        setNameSearch(category);
        axios
            .get(`${URL_CATEGORIES_FILTER}${category}`)
            .then(result => setDisplayMeal(result.data.meals))
            .catch(error => console.error(error));
    }

    const restart = () => {
        inputElement.current.value = "";
        setDisplayMeal([]);
    }

    useEffect(() => {
        searchCategories();
    }, []);

    return (
        <div className="flex width">
            <div className="left">
                <input
                    type="text"
                    placeholder="Search ..."
                    className="filterInput"
                    ref={inputElement}
                    onChange={searchFilter}
                />
                <div className="flex">
                    <div className="category">
                        <h2>Category</h2>
                        {categories.map(category => (
                            <li className="listCategory" key={category.idCategory} onClick={(e)=> clickCategory(e)}>
                                {category.strCategory}
                            </li>
                        ))}
                    </div>
                </div>
            </div>
            <div className="right">
                <div className="flex column">
                    <div className="img-default">
                        {( displayMeal.length === 0 || nameSearch.length === 0) &&
                            <>
                                <p>Go found meals !</p>
                                <img src={img} alt="cook"/>
                            </>
                        }
                    </div>
                    <div className="cancel">
                        {(displayMeal.length !== 0 && nameSearch.length !==0) &&
                            <div className="display">
                                <button
                                    onClick={() => restart()}>Cancer the research
                                </button>
                                <h1 className="nameCategory">{nameSearch}</h1>
                            </div>
                        }
                    </div>
                    <div className="category-card">
                        {(displayMeal.length !== 0 && nameSearch.length !==0 && displayMeal !== null) &&
                            displayMeal.map(meal => (
                                <Card dataMeal={meal} key={meal.strMeal}/>
                            ))
                        }
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Search;