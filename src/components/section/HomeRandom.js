import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Card from '../cards/Card';

const HomeRandom = () => {
    const [mealRandom, setMealRandom] = useState([]);
    const url = 'https://www.themealdb.com/api/json/v1/1/random.php'

    const generateMeal = () => {
        axios
            .get(url)
            .then((result) => { setMealRandom(result.data.meals); })
            .catch(err => {console.log(err);});
    }

    useEffect(() => {
        generateMeal()
    }, []);

    return (
        <div className="home-latest_container">
            <div className="title"> / Random Meals</div>
            <div className="card-container">
                {mealRandom.map(meal => (
                     <Card dataMeal={meal} key={meal.strMeal}/>
                ))}
                <button onClick={() => generateMeal()}>Click another meal</button>
            </div>
        </div>
    );
};

export default HomeRandom;