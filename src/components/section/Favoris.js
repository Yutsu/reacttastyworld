import React, { useCallback, useContext, useEffect } from 'react';
import { FaTimes } from 'react-icons/fa';
import Card from '../cards/Card';
import { NavContext } from '../global/NavContext';
import axios from 'axios';

const localFav = localStorage.getItem('idMealFav');
let idMealFav = JSON.parse(localFav);
const mealTab = [];

const Favoris = () => {
    // const [mealFav, setMealFav] = useState(idMealFav);
    const {isOpenFav, setIsOpenFav} = useContext(NavContext);

    const idMeals = useCallback(() => {
        if(idMealFav){
            const urlTab = [];
            let URL_MEAL_FAV = "";
            for(let i = 0; i < idMealFav.length; i++){
                URL_MEAL_FAV = `https://www.themealdb.com/api/json/v1/1/lookup.php?i=${idMealFav[i]}`;
                urlTab.push(URL_MEAL_FAV);
            }
            console.log(urlTab)
            for(let i = 0; i < urlTab.length; i++){
                axios
                    .get(urlTab[i])
                    .then((result) => { mealTab.push(result.data.meals); })
                    .catch(err => {console.log(err);});
            }
            console.log(mealTab)
            // setMealFav(mealFav);
            // console.log(mealFav)
            // return mealTab;
        }
    }, []);

    useEffect(() => {
        localStorage.setItem("idMealFav", JSON.stringify(idMealFav));
        idMeals();
    }, [idMeals])

    const toggle = () => {
        setIsOpenFav(!isOpenFav);
    }

    const style= {
        textalign: "center",
        marginTop: "40px"
    }

    const noDisplay = () => {
        setIsOpenFav(!isOpenFav);
    }

    return (
        <div className={isOpenFav ? 'favoris active' : 'favoris close'}>
            <div className="favoris-container">
                <div className="title">
                    <p> Your favoris meals </p>
                    <div className="favoris-close" onClick={toggle}>
                        <FaTimes />
                    </div>
                </div>
                <div className="line"></div>
                <div className="favoris-container-card">
                    {mealTab.length === 0 && <h1 style={style}>No meal added</h1>}
                    {mealTab.map(meals => (
                        meals.map(meal => (
                            <div onClick={noDisplay} key={meal} className="fav-click">
                                <Card dataMeal={meal} key={meal}/>
                            </div>
                        ))
                    ))}
                </div>
            </div>
        </div>
    );
};

export default Favoris;