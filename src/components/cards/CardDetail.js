import React, { useState, useEffect } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import axios from 'axios';
import ReactPlayer from "react-player";
import { AiFillStar } from 'react-icons/ai';
import { saveLocalMeals, removeLocalMeals } from '../global/LocalStorage.js';
import { saveMealFav, removeMealFav } from '../global/LocalStorage.js';

const CardDetail = () => {
    let history = useHistory();
    let { slug } = useParams();
    const [mealDetail, setMealDetail] = useState([]);
    const URL_MEAL_DETAIL = `https://www.themealdb.com/api/json/v1/1/lookup.php?i=${slug}`;

    let favCard = JSON.parse(localStorage.getItem('cardFav'));
    let fav = [];

    const checkFavMeal = () => {
        if(!favCard) {
            fav = false;
        }
        if(favCard){
            for (var i = 0; i < favCard.length; i++) {
                if(favCard[i].id !== slug) {
                    fav = false;
                }
                else if(favCard[i].bool === true && favCard[i].id === slug){
                    fav = true;
                    break;
                }
                else {
                    fav = false;
                }
            }
            return fav;
        }
    }
    checkFavMeal();

    const [isFav, setIsFav] = useState(fav);
    const ingredientMeal = (meal) => {
        const ingredients = [];
        for (let i = 1; i <= 20; i++){
            if(meal[`strIngredient${i}`]){
                ingredients.push(`
                    ${meal[`strIngredient${i}`]} - ${meal[`strMeasure${i}`]}
                `)
            } else {
                break;
            }
        }

        return ingredients.map((ingredient, i) => (
            <ul key={i}>
                <li>{ingredient}</li>
            </ul>
        ));
    }

    const toggle = () => {
        const star = document.querySelector('svg');
        if(isFav === false){
            saveMealFav({id: slug, bool:true});
            star.classList.add('active');
            saveLocalMeals(slug);
            setIsFav(true);
        }
        else {
            for (var i = 0; i < favCard.length; i++) {
                if(favCard[i].id === slug && favCard[i].bool === true) {
                    removeMealFav(favCard[i].id);
                    break;
                }
            }
            removeLocalMeals(slug);
            star.classList.remove('active');
            setIsFav(false);
        }
    }

    const check = (mealCheck) => {
        if(mealCheck == null){
            return "X";
        }
        return (mealCheck).split(',').join(', ');
    }

    useEffect(() => {
        axios
            .get(URL_MEAL_DETAIL)
            .then((result) => { setMealDetail(result.data.meals); })
            .catch(err => {console.log(err);});
    }, [URL_MEAL_DETAIL])

    return (
        <div className="cardDetail">
            <div className="retour">
                <button onClick={history.goBack}>Retour</button>
            </div>
            <div className="cardDetail-container">
                {mealDetail.map(meal => (
                    <div key={meal.idMeal}>
                        <div className="flex">
                            <div className="leftSide">
                                <img src={meal.strMealThumb} alt="meal detail"/>
                            </div>
                            <div className="rightSide">
                                <h1>{meal.strMeal}</h1>
                                <p><u>Category:</u> {meal.strCategory}</p>
                                <p><u>Country:</u> {meal.strArea}</p>
                                <p><u>Tags:</u> {check(meal.strTags)}</p>
                                <p>
                                    <AiFillStar className={(isFav) ? 'svg active' : 'svg'} onClick={toggle}/>
                                </p>
                            </div>
                        </div>
                        <div className="ingredients">
                            <u>Ingredients:</u>
                            {ingredientMeal(meal)}
                        </div>
                        <div className="description">
                            <p>
                                <u>Instructions:</u> {meal.strInstructions}
                            </p>
                        </div>
                        <div className="card-youtube">
                            <ReactPlayer url={meal.strYoutube} controls={true}/>
                        </div>
                        <div className="link-container">
                            <p className="link-title">Link: </p>
                            <a href={meal.strSource} rel="noreferrer" className="link"target="_blank">
                                {!(meal.strSource) && <p>X</p>}
                                <p>{meal.strSource}</p>
                            </a>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
};

export default CardDetail;