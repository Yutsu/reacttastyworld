import React, {useState, useEffect} from 'react';
import { Link } from 'react-router-dom';

const Card = ({dataMeal}) => {
    const { strMeal, strMealThumb, idMeal } = dataMeal;
    const [nameMeal, setNameMeal] = useState("");

    const title = (strMeal) => {
        setNameMeal(strMeal);
        if(strMeal.length > 30){
            setNameMeal(`${strMeal.substring(0, 30)}...`);
        }
    }

    useEffect(() => {
        title(strMeal)
    }, [strMeal])

    return (
        <div className="card">
            <Link to={"/meal/" + idMeal}>
                <img src={strMealThumb} alt="Meal random from theMealDB api"/>
            </Link>
            <h3>{nameMeal}</h3>
        </div>
    );
};

export default Card;