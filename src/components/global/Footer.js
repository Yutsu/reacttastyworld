import React from 'react';
import {
    FaLinkedin,
    FaTwitter,
    FaYoutube,
    FaInstagram
} from 'react-icons/fa';

const Footer = () => {
    return (
        <footer>
            <div className="footer-container">
                <ul>
                    <li><h3>About us</h3></li>
                    <li>Testimonials</li>
                    <li>Investors</li>
                    <li>Restaurants</li>
                    <li>Partnership</li>
                    <li>Terms of Service</li>
                </ul>
                <ul>
                    <li><h3>Products</h3></li>
                    <li>Farmers</li>
                    <li>Supermarket</li>
                    <li>Local Producer</li>
                    <li>Delivery</li>
                </ul>
                <ul>
                    <li><h3>Contact us</h3></li>
                    <li>Support</li>
                    <li>Sponsorships</li>
                    <li>Destinations</li>
                </ul>
                <ul>
                    <li><h3>Social Media</h3></li>
                    <li><FaTwitter/> Twitter</li>
                    <li><FaInstagram/> Instagram</li>
                    <li><FaYoutube/> Youtube</li>
                    <li><FaLinkedin/> Linkedin</li>
                </ul>
            </div>
        </footer>
    );
};

export default Footer;