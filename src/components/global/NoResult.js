import React from 'react';
import img from '../../assets/no-data.svg'

const NoResult = () => {
    return (
        <div>
            <h2>No meal for this one!</h2>
            <img src={img} className="mg-t size-img" alt="no found for this meal"/>
        </div>
    );
};

export default NoResult;