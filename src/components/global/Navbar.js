import React, { useState, useMemo } from 'react';
import { FaBars, FaTimes } from 'react-icons/fa';
import { AiFillStar } from 'react-icons/ai';
import { NavLink } from 'react-router-dom';
import Favoris from '../section/Favoris';
import { NavContext } from '../global/NavContext';

const Navbar = () => {
    const [isOpen, setIsOpen] = useState(false);
    const [isOpenFav, setIsOpenFav] = useState(false);

    const providerValue = useMemo(() =>
        ({isOpenFav, setIsOpenFav}), [isOpenFav, setIsOpenFav]
    );

    const toggle = () => {
        window.scrollTo({
            top: 0,
            left: 0,
            behavior: "smooth"
        });
        setIsOpen(!isOpen);
    }

    const toggleFav = () => {
        setIsOpenFav(!isOpenFav);
    }

    return (
        <nav>
            <div className="nav-container">
                <div className="nav-left">
                    <div className="nav-open" onClick={toggle}>
                        <FaBars />
                    </div>
                    <ul className={isOpen ? 'nav-menu active' : 'nav-menu'}>
                        <div className="nav-close" onClick={toggle}>
                            <FaTimes />
                        </div>
                        <NavLink className="nav-logo" to="/" onClick={toggle}>
                            TASTYWORLD
                        </NavLink>
                        <NavLink className="nav-link" to="/meals" onClick={toggle}>
                            Meals
                        </NavLink>
                        <NavLink className="nav-link" to="/country" onClick={toggle}>
                            Country
                        </NavLink>
                        <NavLink className="nav-link" to="/alphabet" onClick={toggle}>
                            Alphabet
                        </NavLink>
                    </ul>
                </div>
                <div className="nav-right" onClick={toggleFav}>
                    <AiFillStar/>
                </div>
            </div>
            <NavContext.Provider value={providerValue}>
                <Favoris/>
            </NavContext.Provider>
        </nav>
    );
};

export default Navbar;