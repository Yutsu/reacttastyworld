const checkLocalMeal = () => {
    let meals;
    let storage = localStorage.getItem('idMealFav');
    storage === null ? meals = [] : meals = JSON.parse(storage);
    return meals;
}

export const saveLocalMeals = (idMealFav) => {
    let meals = checkLocalMeal();
    meals.push(idMealFav);
    localStorage.setItem('idMealFav', JSON.stringify(meals));
}

export const removeLocalMeals = (idMealFav) => {
    let meals = checkLocalMeal();
    meals.splice(meals.indexOf(idMealFav), 1);
    localStorage.setItem('idMealFav', JSON.stringify(meals))
}


const checkMealFav = () => {
    let fav;
    let storage = localStorage.getItem('cardFav');
    storage === null ? fav = [] : fav = JSON.parse(storage);
    return fav;
}

export const saveMealFav = (idMeal) => {
    let fav = checkMealFav();
    fav.push(idMeal);
    localStorage.setItem('cardFav', JSON.stringify(fav));
}

export const removeMealFav = (idMeal) => {
    let fav = checkMealFav();
    let index = fav.map(i => i.id).indexOf(idMeal);
    fav.splice(index, 1);
    localStorage.setItem('cardFav', JSON.stringify(fav))
}
